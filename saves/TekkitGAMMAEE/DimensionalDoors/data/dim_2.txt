{
  "SAVE_DATA_VERSION_ID_INSTANCE": 982405775,
  "ID": 2,
  "IsDungeon": true,
  "IsFilled": true,
  "Depth": 1,
  "PackDepth": 0,
  "ParentID": 0,
  "RootID": 0,
  "DungeonData": {
    "Weight": 100,
    "IsOpen": false,
    "IsInternal": true,
    "SchematicPath": "/schematics/ruins/hub_hallwayTrapRooms1_closed_100.schematic",
    "SchematicName": "hub_hallwayTrapRooms1_closed_100",
    "DungeonTypeName": "HUB",
    "DungeonPackName": "RUINS"
  },
  "Origin": {
    "x": 30,
    "y": 62,
    "z": 20
  },
  "Orientation": 0,
  "ChildIDs": [
    3
  ],
  "Links": [
    {
      "source": {
        "x": 30,
        "y": 62,
        "z": 20,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "destination": {
          "x": 147,
          "y": 62,
          "z": 25,
          "dimension": 0
        },
        "linkType": 7
      },
      "orientation": 0,
      "children": []
    },
    {
      "source": {
        "x": 28,
        "y": 62,
        "z": 33,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "linkType": 2
      },
      "orientation": 1,
      "children": []
    },
    {
      "source": {
        "x": 28,
        "y": 62,
        "z": 29,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "linkType": 2
      },
      "orientation": 3,
      "children": []
    },
    {
      "source": {
        "x": 28,
        "y": 62,
        "z": 27,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "destination": {
          "x": 24,
          "y": 62,
          "z": 287,
          "dimension": 3
        },
        "linkType": 2
      },
      "orientation": 1,
      "children": []
    },
    {
      "source": {
        "x": 28,
        "y": 62,
        "z": 23,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "linkType": 2
      },
      "orientation": 3,
      "children": []
    },
    {
      "source": {
        "x": 30,
        "y": 62,
        "z": 31,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "linkType": 2
      },
      "orientation": 0,
      "children": []
    },
    {
      "source": {
        "x": 30,
        "y": 62,
        "z": 25,
        "dimension": 2
      },
      "parent": {
        "x": -1,
        "y": -1,
        "z": -1
      },
      "tail": {
        "linkType": 2
      },
      "orientation": 0,
      "children": []
    }
  ],
  "Tails": []
}